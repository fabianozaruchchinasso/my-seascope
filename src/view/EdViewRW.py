#!/usr/bin/env python

# Copyright (c) 2010 Anil Kumar
# All rights reserved.
#
# License: BSD 

import os
import re

from PyQt4 import QtGui
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import subprocess

from EdView import *

# inherit from EditView, add editing support.
class EditorViewRW(EditorView):
	sig_file_modified = pyqtSignal(bool)
	def __init__(self, parent=None):
		EditorView.__init__(self, parent)
		self.setAutoCompletionSource(QsciScintilla.AcsAll)
		self.setAutoCompletionThreshold(3)
		self.setAutoCompletionUseSingle(QsciScintilla.AcusNever)
		self.setAutoCompletionReplaceWord(True)
		self.setAutoIndent(True)
		self.setBraceMatching(QsciScintilla.SloppyBraceMatch)
		# use default settings.
		EditorView.ed_settings_1(self)

	def open_file(self, filename):
		self.open_file_begin(filename)

		## Show this file in the editor
		data = open(filename).read()
		try:
			data = data.decode("UTF-8")
		except:
			pass
		self.setText(data)

		## process for modifiled.
		self.setModified(False)
		self.modificationChanged.connect(self.modifiedChanged)
		
		## support edit 
		self.setReadOnly(False)

		self.open_file_end()

	def modifiedChanged(self):
		self.sig_file_modified.emit(self.isModified())

	def get_diff(self, filename):
		fobj = open('/tmp/buffer', 'w')
		if (not fobj.closed):
				fobj.write(str(self.text().toUtf8()))
				fobj.flush()
				fobj.close()
				cmd = 'diff -u /tmp/buffer {0}'.format(filename)
				proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
				(out, err) = proc.communicate()
				#for o in out.split('\n'):
				#	print("program output:", o)
				return out

	# FIXME: too simple for big files or remote files.	
	def save_file(self, filename):
		if(self.isModified()):
			fobj = open(filename, 'w')
			if (not fobj.closed):
				fobj.write(str(self.text().toUtf8()))
				fobj.flush()
				fobj.close()
				self.setModified(False)

class EditorPageRW(EditorPage):
	def new_editor_view(self):
		return EditorViewRW(self)


class EditorBookRW(EditorBook):

	def new_editor_page(self):
		return EditorPageRW()

	def save_file_at_inx(self, inx):
		if inx < 0:
			return
		page = self.widget(inx)
		filename = page.get_filename()
		if filename:
			page.ev.save_file(filename)
			self.tabBar().setTabTextColor(inx, Qt.black)
			page.fcv.rerun(filename)
		return filename

	def save_current_page(self):
		inx = self.currentIndex()
		return self.save_file_at_inx(inx)

	def get_diff(self, filename):
		inx = self.currentIndex()
		if inx < 0:
			return
		page = self.widget(inx)
		fname = page.get_filename()
		if fname != filename:
			print("wrong file")
		return page.ev.get_diff(filename)

	def removeTab(self, inx):
		page = self.widget(self.currentIndex())
		if page.ev.isModified():
			if DialogManager.show_yes_no("Do you want to save file ?"):
				self.save_current_page()
		super(EditorBookRW, self).removeTab(inx)

	def save_tab_list(self, inx_list):
		for inx in inx_list:
			self.save_file_at_inx(inx)

	def save_all_file(self):
		inx_list = range(self.count())
		self.save_tab_list(inx_list)

	def page_modified_cb(self, isModifiled):
		inx = self.currentIndex()
		filename = self.tabText(inx)
		# Sign modified.
		if isModifiled:
			self.tabBar().setTabTextColor(inx, Qt.red)
		else:
			self.tabBar().setTabTextColor(inx, Qt.black)

	def close_cb(self, inx):
		page = self.widget(self.currentIndex())
		if page.ev.isModified():
			if DialogManager.show_yes_no("Do you want to save file ?"):
				self.save_current_page()
		self.removeTab(inx)

	def has_modified_file(self, inx_list):
		for i in inx_list:
			page = self.widget(i)
			if page.ev.isModified():
				return True
		return False

	def close_list_common(self, type):
		inx_list = self.tab_list(self.currentIndex(), type)
		if len(inx_list) == 0:
			return
		if self.has_modified_file(inx_list):
			msg = 'Closing all %s.\nSave changes ?' % type
			if DialogManager.show_yes_no(msg):
				self.save_all_file()
			self.remove_tab_list(inx_list)
		else:
			msg = 'Close all %s ?' % type
			if not DialogManager.show_yes_no(msg):
				return
			self.remove_tab_list(inx_list)

	def show_file_line(self, filename, line, hist=True):
		EditorBook.show_file_line(self, filename, line, hist=hist)
		page = self.currentWidget()
		# modified signal callback
		page.ev.sig_file_modified.connect(self.page_modified_cb)
		
	# redo editing callback
	def redo_edit_cb(self):
		ed = self.currentWidget()
		if ed:
			ed.ev.redo()

	# undo editing callback
	def undo_edit_cb(self):
		ed = self.currentWidget()
		if ed:
			ed.ev.undo()

	# edting callbacks
	def cut_edit_cb(self):
		ed = self.currentWidget()
		if ed:
			ed.ev.cut()

	def paste_edit_cb(self):
		ed = self.currentWidget()
		if ed:
			ed.ev.paste()

	def replace_cb(self):
		res = DialogManager.show_replace_dialog(self.get_current_word())
		if (res == None):
			return
		(text, text_repl, opt) = res
		if (text == None):
			return
		self.f_text = text
		self.f_text_repl = text_repl
		self.f_opt = opt
		if(opt['rpl_all']):
			self.replace_all()
			return
		self.replace_text(opt['cursor'], opt['fw'])

	def replace_next_cb(self):
		self.replace_text(True, True)

	def replace_prev_cb(self):
		self.replace_text(True, False)

	def replace_text(self, from_cursor, is_fw):
		ed = self.find_text(from_cursor, is_fw)
		if ed == None:
			return
		ed.ev.replace(self.f_text_repl)

	def replace_all(self):
		if (self.f_text == None):
			return
		ed = self.currentWidget()
		if not ed:
			return
		opt = self.f_opt
		ed.ev.findFirst(self.f_text, opt['re'], opt['cs'], opt['wo'], False, True, 0, 0)
		ed.ev.replace(self.f_text_repl)
		while True:
			ed.ev.replace(self.f_text_repl)
			if (not ed.ev.findNext()):
				break
		self.highlight_word(self.f_text_repl)

	def format_file_at_inx(self, inx):
		if inx < 0:
			return
		page = self.widget(inx)
		cmd="clang-format-7"
		res= os.system(cmd + ' --version')
		if res != 0:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Information)
			msg.setText(cmd + " is not available.")
			msg.setInformativeText("Please install with: \napt install " + cmd)
			msg.setWindowTitle("Missing Clang-Format")
			#msg.setDetailedText("The details are as follows:")
			msg.setStandardButtons(QMessageBox.Ok)
			#msg.buttonClicked.connect(msgbtn)
			retval = msg.exec_()
			return 1
		filename = page.get_filename()
		if filename:
			(line, idx) = page.ev.getCursorPosition()
			cmd = 'clang-format-7 -style=file {0} > /tmp/file;diff {0} /tmp/file | patch {0}'.format(filename)
			os.system(cmd)
			## Show this file in the editor
			page.ev.setText(open(filename).read())
			page.ev.open_file_end()
			page.ev.goto_line(line + 1)
			return 0

	def file_format_cb(self):
		inx = self.currentIndex()
		self.format_file_at_inx(inx)

	def format_tab_list(self, inx_list):
		for inx in inx_list:
			self.format_file_at_inx(inx)
			res = self.save_file_at_inx(inx)
			if res:
				break

	def format_all_file(self):
		inx_list = range(self.count())
		self.format_tab_list(inx_list)
